<!-- Modal -->
<div class="modal fade" id="{!! $id !!}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Attribute</h5>
                
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    {{ html()->label("attribute's name")->class('col-md-2 form-control-label')->for('attr_name') }}
                    <div class="col-md-7">
                        {{ html()->text('attr_name')
                            ->class('form-control')
                            ->placeholder('attribute name')
                         }}
                    </div><!--col-->
                </div><!--form-group-->
                <div class="form-group row">
                    {{ html()->label('Type')->class('col-md-2 form-control-label')->for('type') }}
                    <div class="col-md-10">
                        <select name="type" id="type" class="form-control">
                            @foreach (['text','number','rich'] as $type)
                                <option value="{!! $type !!}">{{ $type }}</option>
                            @endforeach
                        </select>
                    </div><!--col-->
                </div><!--form-group-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="btn_save_attr" data-dismiss="modal">Save changes</button>
            </div>
        </div>
    </div>
</div>