@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
    @php
        extract($params);
    @endphp
    {{ html()->form('POST', route('admin.article.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Article Management
                        <small class="text-muted">Article Category</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            
            <hr/>
            
            <div class="row mt-4 mb-4">
                <div class="col-md-8">
                    <div class="form-group row">
                        {{ html()->label('Parent Category')->class('col-md-2 form-control-label')->for('category_id') }}
                        
                        <div class="col-md-10">
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">--</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Title')->class('col-md-2 form-control-label')->for('title') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->placeholder('name of article')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Slug')->class('col-md-2 form-control-label')->for('slug') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('slug')
                                ->class('form-control')
                                ->placeholder('slug of article')
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('thumb')->class('col-md-2 form-control-label')->for('thumb') }}
                        <div class="col-md-8">
                            {{ html()->text('thumb')
                                ->class('form-control')
                                ->placeholder('http://example.jpg')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Content')->class('col-md-2 form-control-label')->for('content') }}
                        <div class="col-md-10">
                            {{ html()->textarea('content')
                                ->class('form-control')
                                ->placeholder('content of article')
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
                <div class="col-md-4">
                
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
<script src="/js/backend/ecom/Article.js"></script>
@include('backend.includes.texteditor_script')
<script>
    CKEDITOR.replace('content');
</script>
@endpush
