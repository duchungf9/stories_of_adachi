<?php
namespace App\Http\Controllers\Backend\Category;

use App\Models\Category;
use App\Http\Controllers\Backend\AdminController;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
class CategoryController extends AdminController
{
	protected $categoryModel;
	function __construct(){
		if(!$this->categoryModel instanceof Category){
			$this->categoryModel = new Category();
		}
	}
	
	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$params['categories'] = $this->categoryModel->paginate(10);
		return view('backend.category.index',compact('params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$params = [];
		$params['categories'] = $this->categoryModel->paginate(10);
		return view('backend.category.store',compact('params'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validateResults = $this->validateRequest();
		if(is_array($validateResults)){
			return redirect()->route('admin.category.create')->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		if($validateResults===true){
			$insert = ($this->categoryModel->insertCat(request()->except("_token")));
			if($insert){
				return redirect()->route('admin.category.index');
			}
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
		$params = [];
		$params['categories'] = $this->categoryModel->paginate(10);
		$params['category'] = $this->categoryModel->where('id',$id)->first();
		return view('backend.category.show',compact('params'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$Cat = $this->categoryModel->where('id',$id)->first();
		if($Cat==null){
			return redirect()->route('admin.category.index');
		}
		$validateResults = $this->validateRequest($Cat->id);
		if(is_array($validateResults)){
			return redirect()->route('admin.category.edit',['id'=>$Cat->id])->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		$update = $this->categoryModel->updateCat(request()->except(["_token","_method"]),$Cat);
		if($update)
		{
			return redirect()->route('admin.category.edit',['id'=>$Cat->id]);
		}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	/**
	 * @param array $request
	 * @param array $rules
	 * @param array $messages
	 *
	 * @return mixed
	 */
	private function validator(array $request,array $rules, array $messages = []){
		return Validator::make($request,$rules,$messages);
	}
	
	/**
	 * @return mixed
	 */
	private function validateRequest($id=null){
		$rules = $this->categoryModel->getRules(request()->method(),$id);
		$validator = $this->validator(request()->all(),$rules);
		if($validator->fails() == true){
			return $validator->messages()->messages();
		}
		return $validator->passes();
	}
}
