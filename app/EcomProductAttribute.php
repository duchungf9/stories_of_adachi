<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcomProductAttribute extends Model
{
	protected $table = 'ecom_product_attribute';
	public function mapProductToAttrs(EcomProduct $model, array $attrIds)
	{
	    foreach($attrIds as $id){
            $EcomProductAttr = $this->where('ecom_product_id',$model->id)->where('ecom_attribute_value_id',$id)->first();
            if($EcomProductAttr == null ){
				$EcomProductAttr = new $this;
			}
            $EcomProductAttr->ecom_product_id = $model->id;
            $EcomProductAttr->ecom_attribute_value_id = $id;
            $EcomProductAttr->save();

        }

	}

    public function EcomAttrValue(){
	    return $this->belongsTo(EcomAttributeValue::class,'ecom_attribute_value_id','id');
    }
}
