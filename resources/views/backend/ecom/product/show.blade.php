@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
    @php
        extract($params);
    @endphp
    {{ html()->form('POST', route('admin.ecom_product.update',['id'=>$product]))->class('form-horizontal')->open() }}
    {{ method_field('PATCH') }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Product Management
                        <small class="text-muted">Edit Product</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            
            <hr/>
            
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label('Parent Category')->class('col-md-2 form-control-label')->for('category_id') }}
                        
                        <div class="col-md-10">
                            <select name="category_id" id="category_id" class="form-control" required="required">
                                <option value="">--</option>
                                @foreach ($categories as $categoryChild)
                                    <option value="{{ $categoryChild->id }}"
                                            @if($categoryChild->id == $category->id)
                                            selected
                                            @endif
                                    >{{ $categoryChild->name }}</option>
                                    @if(count($categoryChild->child_ecom_category)>0)
                                        @foreach($categoryChild->child_ecom_category as $child)
                                            <option value="{!! $child->id !!}"
                                                    @if($child->id == $category->id)
                                                    selected
                                                    @endif
                                            >-{!! $child->name !!}</option>
                                        
                                        @endforeach
                                    
                                    @endif
                                @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Name')->class('col-md-2 form-control-label')->for('name') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('name')
                                ->class('form-control')
                                ->placeholder('name of category')
                                ->attribute('maxlength', 191)
                                 ->value($category->name)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    <div class="form-group row">
                        {{ html()->label('Alias')->class('col-md-2 form-control-label')->for('alias') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('alias')
                                ->class('form-control')
                                ->placeholder('alias of category')
                                ->attribute('maxlength', 191)
                                ->value($category->alias)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label("SKU")->class('col-md-2 form-control-label')->for('sku') }}
                        
                        <div class="col-md-10">
                            {{ html()->input('number','sku')
                                ->class('form-control')
                                ->value($product->sku)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    {{ html()->label('Active')->class('col-md-2 form-control-label')->for('is_active') }}
                    
                    <div class="col-md-10">
                        <label class="switch switch-3d switch-primary">
                            {{ html()->checkbox('is_active', true, '1')->class('switch-input') }}
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div><!--col-->
                    {{ html()->label('Visibility')->class('col-md-2 form-control-label')->for('visibility') }}
                    
                    <div class="col-md-10">
                        <label class="switch switch-3d switch-primary">
                            {{ html()->checkbox('visibility', true, '1')->class('switch-input') }}
                            <span class="switch-label"></span>
                            <span class="switch-handle"></span>
                        </label>
                    </div><!--col-->
                </div><!--form-group-->
            </div><!--col-->
        </div><!--row-->
    </div><!--card-body-->
    
    <div class="card-footer clearfix">
        <span id="ecom_product_attribute">
            @foreach($product->attrs as $attr)
                <div class="form-group row">
                {{ html()->label($attr->name)->class('col-md-2 form-control-label')->for($attr->name."[value]") }}
            
                    <div class="col-md-7">
                        @if($attr->type == 'text')
                            {{ html()->text($attr->name."[value]")
                            ->class('form-control')
                            ->value($attr->value)
                            ->required()
                            }}
                        @elseif($attr->type=='number')
                            {{ html()->text($attr->name."[value]")
                            ->class('form-control')
                            ->value($attr->value)
                            ->required()
                            }}
                        @elseif($attr->type=='rich')
                            {{ html()->textarea($attr->name."[value]")
                            ->class('form-control')
                            ->value($attr->value)
                            ->required()
                            }}
                        @endif
                
                        <input type="hidden" name="{!! $attr->name !!}[type]" value="{!! $attr->type !!}">
                     </div><!--col-->
                 </div><!--form-group-->
            @endforeach
        </span>
        
        <div class="col text-right">
            <p class="btn btn-warning btn-sm pull-right" type="submit" style="color: #151b1e;" id="moreAttribute"
               data-toggle="modal" data-target="#addAttr">+ attribute(s)</p>
        </div><!--col-->
    </div><!--card-footer-->
    
    <div class="card-footer clearfix">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
            </div><!--col-->
            
            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.update')) }}
            </div><!--col-->
        </div><!--row-->
    </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
<script src="/js/backend/ecom/EcomProduct.js"></script>
<script>
    @foreach($product->attrs as $attr)
        @if($attr->type == 'rich')
            CKEDITOR.replace("{!! $attr->name !!}"+"[value]");
        @endif
    @endforeach
</script>
@endpush

@include('backend.includes.widgets.modal',['id'=>'addAttr'])

