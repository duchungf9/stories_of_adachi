<?php
/**
 * Created by PhpStorm.
 * User: duchu
 * Date: 1/5/2018
 * Time: 9:18 AM
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Backend\AdminController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class ApiController extends AdminController {
	function __construct(){
	
	}
	
	public function upload()
	{
		
		echo $this->uploadAndGetPath(request()->file('file'));
	}
	
}