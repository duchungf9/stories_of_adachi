<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcomCategorySpecAttr extends Model
{
	protected $table = 'ecom_category_spec_attr';
	
	public function category(){
		return $this->belongsTo(EcomCategory::class,'ecom_cat_id');
	}
}
