<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');
Route::any('api/upload','\App\Http\Controllers\Api\ApiController@upload')->name('api.upload');

Route::get('ecom_category/list','\App\Http\Controllers\Backend\Ecom\EcomCategoryController@getList')->name('ecom_category.list');
Route::resource('ecom_category', '\App\Http\Controllers\Backend\Ecom\EcomCategoryController');
Route::resource('ecom_product', '\App\Http\Controllers\Backend\Ecom\EcomProductController');
Route::resource('article', '\App\Http\Controllers\Backend\Article\ArticleController');
Route::resource('category', '\App\Http\Controllers\Backend\Category\CategoryController');
