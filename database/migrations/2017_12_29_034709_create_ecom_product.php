<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcomProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ecom_product', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('alias')->nullable();
			$table->integer('category_id')->nullable();
			$table->integer('sku')->nullable();
			$table->integer('is_active')->nullable();
			$table->integer('sort_order')->nullable();
			$table->integer('visibility')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
