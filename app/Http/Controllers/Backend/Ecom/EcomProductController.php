<?php

namespace App\Http\Controllers\Backend\Ecom;

use App\EcomCategory;
use App\EcomProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class EcomProductController extends Controller
{
    protected $ecomCat;
    protected $ecomProduct;

    function __construct()
    {
        if (!$this->ecomCat instanceof EcomCategory) {
            $this->ecomCat = new EcomCategory();
        }
        if (!$this->ecomProduct instanceof EcomProduct) {
            $this->ecomProduct = new EcomProduct();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['products'] = EcomProduct::getListProducts(['is_active' => 1], 10);
        return view('backend.ecom.product.index', compact('params'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params['products'] = EcomProduct::getListProducts(['is_active' => 1], 10);
        $params['categories'] = EcomCategory::getListCategories(['is_active' => 1, 'parent_id' => null]);
        return view('backend.ecom.product.store', compact('params'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateResults = $this->validateRequest();
        $validateResults = true;
        if (is_array($validateResults)) {
            return redirect()->route('admin.ecom_product.create')->withErrors($validateResults)->withInput(request()->except("_token"));
        }
        if ($validateResults === true) {
            $insert = ($this->ecomProduct->insertEcomProduct(request()->except("_token")));
            if ($insert) {
                return redirect()->route('admin.ecom_product.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EcomProduct $ecomProduct
     *
     * @return \Illuminate\Http\Response
     */
    public function show(EcomProduct $ecomProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EcomProduct $ecomProduct
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(EcomProduct $ecomProduct)
    {
        $params = [];
        $attributes = $ecomProduct->getCurrentAttrs();
        $ecomProduct->attrs = $attributes;
        $params['categories'] = EcomCategory::getListCategories(['is_active'=>1,'parent_id'=>null]);
        $params['category'] = EcomCategory::where('id',$ecomProduct->category_id)->first();
        $params['product'] = $ecomProduct;
        return view('backend.ecom.product.show',compact('params'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\EcomProduct $ecomProduct
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EcomProduct $ecomProduct)
    {
        $validateResults = $this->validateRequest($ecomProduct->id);
        $validateResults = true;
        if (is_array($validateResults)) {
            return redirect()->route('admin.ecom_product.edit',['product'=>$ecomProduct])->withErrors($validateResults)->withInput(request()->except("_token"));
        }
        if ($validateResults === true) {
            $insert = $this->ecomProduct->insertEcomProduct(request()->except("_token","_method"),$ecomProduct);
            if ($insert) {
                return redirect()->route('admin.ecom_product.edit',['product'=>$ecomProduct]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EcomProduct $ecomProduct
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(EcomProduct $ecomProduct)
    {
        $delete = $this->ecomProduct->where('id', $ecomProduct->id)->delete();
        if ($delete) {
            return redirect()->route('admin.ecom_product.index');

        }
    }


    /**
     * @param null $id
     *
     * @return mixed
     */
    private function validateRequest($id = null)
    {
        $rules = $this->ecomProduct->getRules(request()->method(), $id);
        $validator = $this->validator(request()->all(), $rules);
        if ($validator->fails() == true) {
            return $validator->messages()->messages();
        }
        return $validator->passes();
    }

    /**
     * @param array $request
     * @param array $rules
     * @param array $messages
     *
     * @return mixed
     */
    private function validator(array $request, array $rules, array $messages = [])
    {
        return Validator::make($request, $rules, $messages);
    }
}
