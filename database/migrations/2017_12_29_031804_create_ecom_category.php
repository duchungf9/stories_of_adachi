<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcomCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ecom_category', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('alias')->nullable();
			$table->string('image_1')->nullable();
			$table->string('image_2')->nullable();
			$table->integer('parent_id')->nullable();
			$table->integer('is_active')->default(0)->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
