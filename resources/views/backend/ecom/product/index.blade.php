@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
	<?php
	    extract($params);
	?>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Product Management
                        <small class="text-muted">Actived Product</small>
                    </h4>
                </div><!--col-->
                
                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('admin.ecom_product.create') }}" class="btn btn-success ml-1"
                           data-toggle="tooltip" title="Create New"><i class="fa fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->
            
            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Alias</th>
                                <th>Category</th>
                                <th>Sku</th>
                                <th>Image</th>
                                <th>order</th>
                                <th>visibility</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($products as $product)
                                <tr>
                                    <td>{{ $product->name }}</td>
                                    <td>{{ $product->alias }}</td>
                                    <td>{{ $product->category->name }}</td>
                                    <td>{{ $product->sku }}</td>
                                    <td>{!!   \App\Helpers\General\HtmlHelper::thumbImg($product->getAttrByName('Image'),[100,100],['alt'=>$product->name]) !!}</td>
                                    <td>{{ $product->sort_order }}</td>
                                    <td>{{ $product->visibility }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                                            <a href="{!! route('admin.ecom_product.show',['ecomProduct'=>$product]) !!}" class="btn btn-info"><i
                                                        class="fa fa-search" data-toggle="tooltip" data-placement="top"
                                                        title="" data-original-title="View"></i></a>
                                            <a href="{!! route('admin.ecom_product.edit',['ecomProduct'=>$product]) !!}"
                                               class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip"
                                                                          data-placement="top" title=""
                                                                          data-original-title="Edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="userActions" type="button"
                                                        class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    More
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="userActions">
                                                    {{ html()->form('POST', route('admin.ecom_product.destroy',['ecom_product'=>$product]))->class('form-horizontal')->open() }}
                                                        {{ method_field('DELETE') }}
                                                    {{ form_submit(__('buttons.general.crud.delete')) }}
                                                    {{ html()->form()->close() }}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                    
                    </div>
                </div><!--col-->
                
                <div class="col-5">
                    <div class="float-right">
                        {!! $products->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->

@endsection
