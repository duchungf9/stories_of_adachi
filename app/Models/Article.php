<?php

namespace App\Models;

use App\Category;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $table = 'articles';
	
	public function getRules(string $method,$id=null){
		switch($method)
		{
			case 'GET':
			case 'DELETE':
			{
				return [];
			}
			case 'POST':
			{
				return [
					'title' => 'required',
					'slug'  => 'required|unique:articles',
					'thumb' => 'required|mimes:jpeg,bmp,png',
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				return [
					'title' => 'required',
					'slug'  => 'required|unique:articles,slug,'.$id,
					'thumb' => 'required|mimes:jpeg,bmp,png'
				];
			}
			default:break;
		}
	}

    /**
     * Carbon instance fields
     *
     * @var array
     */
    protected $dates = ['published_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Scope queries to articles that are published
     *
     * @param $query
     */
    public function scopePublished($query)
    {
        return $query->where('published_at', '<=', Carbon::now());
    }

    /**
     * Get the published_at attribute.
     *
     * @param  $date
     * @return string
     */
    public function getPublishedAtAttribute($date)
    {
        return Carbon::parse($date)->format('Y-m-d');
    }

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        return route('article', ['articleSlug' => $this->slug]);
    }
	
	public function insertArticle(array $params, $article = null)
	{
		if($article==null){
			$article = new $this;
		}
		foreach($params as $field=>$value){
			$article->$field = $value;
		}
		return $article->save();
	}
}
