<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

    /**
     * @var array
     */
    protected $fillable = ['content', 'description', 'language_id', 'title'];

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
	

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        return route('page', ['pageSlug' => $this->slug]);
    }
}
