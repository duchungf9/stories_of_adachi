<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class EcomProduct extends Model
{
	protected $table = 'ecom_product';
	protected $paramsList = ['id','name','alias','category_id','sku','is_active','sort_order','visibility','created_at','updated_at'];
	protected $ecomProductAttr;
	protected $ecomAttr;
	protected $ecomAttrValue;
	public function __construct(array $attributes = []){
		parent::__construct($attributes);
		if(!$this->ecomProductAttr instanceof EcomProductAttribute)
		{
			$this->ecomProductAttr = new EcomProductAttribute();
		}
		if(!$this->ecomAttr instanceof EcomAttribute)
		{
			$this->ecomAttr = new EcomAttribute();
		}
		if(!$this->ecomAttrValue instanceof EcomAttributeValue)
		{
			$this->ecomAttrValue = new EcomAttributeValue();
		}
	}
	
	public function getRules(string $method,$id=null){
		switch($method)
		{
			case 'GET':
			case 'DELETE':
			{
				return [];
			}
			case 'POST':
			{
				return [
					'name' => 'required',
					'alias'  => 'required|unique:ecom_product',
					'sku'=>'required|numeric',
					'category_id'=>'required'
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				return [
					'name' => 'required',
					'alias'  => 'required|unique:ecom_product,alias,'.$id,
					'sku'=>'required|numeric',
					'category_id'=>'required'
				];
			}
			default:break;
		}
	}
	public function category()
	{
		return $this->belongsTo(EcomCategory::class,'category_id','id');
	}
	public function attribute(){
        return $this->hasMany(EcomProductAttribute::class,'ecom_product_id','id');
    }
	
	public static function getListProducts(array $filters = [],$paginate = false)
	{
		$query =  new self();
		
		if(!empty($filters)){
			$filter = [];
			foreach($filters as $key=>$value)
			{
				$filter[] = [$key, '=',$value];
			}
			$query =  $query->where($filter);
		}
		if($paginate!=false){
			return $query->paginate($paginate);
		}
		return $query->get();
	}
	
	public function insertEcomProduct(array $params,EcomProduct $ecomProduct = null){
		$attributes = [];
		foreach($params as $key=>$param){
			if(!in_array($key,$this->paramsList)){
				unset($params[$key]);
				$attributes[$key] = $param;
			}
		}
		$newEcomProduct = new $this;
        if($ecomProduct!=null){
            $newEcomProduct =  $ecomProduct;
        }
		foreach($params as $key=>$param)
		{
			$newEcomProduct->$key = $param;
		}
		$saveProduct = $newEcomProduct->save();

		if($saveProduct){
            $attrIds = $this->ecomAttr->insertAttr($attributes);
			$attrValueId = $this->ecomAttrValue->insertAttrValue($attributes,$attrIds);
			$this->ecomProductAttr->mapProductToAttrs($newEcomProduct,$attrIds);
			return true;
		}
		
	}
	
	/**
	 * @param \App\EcomProduct $ecomProduct
	 *
	 * @return \Illuminate\Support\Collection
	 */
	public function getCurrentAttrs()
	{
		$attributes = collect([]);
		if(count($this->attribute)>0){
			foreach ($this->attribute as $key => $attribute) {
				$newAttr = new \stdClass();
				$attrName = $attribute->EcomAttrValue->EcomAttr->name;
				$attrVal = $attribute->EcomAttrValue->value;
				$attrType = $attribute->EcomAttrValue->EcomAttr->type;
				$newAttr->name = $attrName;
				$newAttr->value = $attrVal;
				$newAttr->type = $attrType;
				$attributes->push($newAttr);
			}
		}
		return $attributes;
	}
	
	/**
	 * @param string $name
	 *
	 * @return mixed
	 */
	public function getAttrByName(string $name)
	{
		$attrs = $this->getCurrentAttrs();
		return $attrs->where("name",$name)->pluck('value')->first();
	}
	
}
