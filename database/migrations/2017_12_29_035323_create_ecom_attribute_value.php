<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcomAttributeValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ecom_attribute_value', function (Blueprint $table) {
			$table->increments('id');
			$table->integer('ecom_attribute_id')->nullable();
			$table->text('value')->nullable();
			$table->timestamps();
		});    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
