<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['color', 'description','language_id', 'title'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo(Language::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return mixed
     */
    public function paginatedArticles()
    {
        return $this->articles()->published()->orderBy('published_at', 'desc');
    }

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        return route('category', ['categorySlug' => $this->slug]);
    }
	
	/**
	 * @param array $params
	 *
	 * @return bool
	 */
	public function insertCat(array $params)
	{
		$newCat = new $this;
		foreach($params as $field=>$value){
			$this->$field = $value;
		}
		return $this->save();
	}
	
	/**
	 * @param array             $params
	 * @param \App\EcomCategory $category
	 *
	 * @return bool
	 */
	public function updateCat(array $params,Category $category)
	{
		foreach($params as $field=>$value){
			$category->$field = $value;
		}
		return $category->save();
	}
	
	public function getRules(string $method,$id=null){
		switch($method)
		{
			case 'GET':
			case 'DELETE':
			{
				return [];
			}
			case 'POST':
			{
				return [
					'title' => 'required',
					'slug'  => 'required|unique:categories'
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				return [
					'title' => 'required',
					'slug'  => 'required|unique:categories,slug,'.$id,
				];
			}
			default:break;
		}
	}
	
}
