<?php
/**
 * Created by PhpStorm.
 * User: duchu
 * Date: 1/5/2018
 * Time: 9:41 AM
 */
namespace App\Http\Controllers\Backend\Article;

use App\Models\Category;
use App\Http\Controllers\Backend\AdminController;
use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ArticleController extends AdminController{
	protected $articleModel;
	protected $categoryModel;
	public function __construct(){
		if(!$this->articleModel instanceof Article){
			$this->articleModel = new Article();
		}
		if(!$this->categoryModel instanceof Category){
			$this->categoryModel = new Category();
		}
	}
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$params['articles'] = $this->articleModel->paginate(10);
		return view('backend.article.index', compact('params'));
	}
	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$params['categories'] = $this->categoryModel->get();
		return view('backend.article.store', compact('params'));
	}
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$validateResults = $this->validateRequest();
		$validateResults = true;
		if (is_array($validateResults)) {
			return redirect()->route('admin.article.create')->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		if ($validateResults === true) {
			$insert = ($this->articleModel->insertArticle(request()->except("_token")));
			if ($insert) {
				return redirect()->route('admin.ecom_product.index');
			}
		}
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  \App\EcomProduct $ecomProduct
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Article $article)
	{
		$params = [];
		$params['categories'] = $this->categoryModel->get();
		$params['article'] = $article;
		return view('backend.article.show',compact('params'));
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \App\EcomProduct $ecomProduct
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Article $article)
	{
		$validateResults = $this->validateRequest($article->id);
		$validateResults = true;
		if (is_array($validateResults)) {
			return redirect()->route('admin.article.edit',['article'=>$article])->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		if ($validateResults === true) {
			$insert = $this->articleModel->insertArticle(request()->except("_token","_method"),$article);
			if ($insert) {
				return redirect()->route('admin.article.edit',['article'=>$article]);
			}
		}
	}
	
	/**
	 * @param null $id
	 *
	 * @return mixed
	 */
	private function validateRequest($id = null)
	{
		$rules = $this->articleModel->getRules(request()->method(), $id);
		$validator = $this->validator(request()->all(), $rules);
		if ($validator->fails() == true) {
			return $validator->messages()->messages();
		}
		return $validator->passes();
	}
	
	/**
	 * @param array $request
	 * @param array $rules
	 * @param array $messages
	 *
	 * @return mixed
	 */
	private function validator(array $request, array $rules, array $messages = [])
	{
		return Validator::make($request, $rules, $messages);
	}
	
	
	
}
