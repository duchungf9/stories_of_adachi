/**
 * Created by duchu on 1/3/2018.
 */




$("input[name=name]").on('change',function(){
    var value = $(this).val();
    $("input[name=alias]").val(slugify(value));
});

$("#btn_save_attr").click(function(){
    var attrName = $("input[name=attr_name]").val();
    var attrType = $("select[name=type]").val();
    var functionName = "render_"+attrType+"_AttrHtml";
    $("#ecom_product_attribute").append(window[functionName](attrName));
    if(attrType=='rich'){
        richtext("rich_"+slugify(attrName));
    }
});

function render_text_AttrHtml(attrName)
{
    var html =  "";
    html +=  "<div class='form-group row'>" +
                "<label class='col-md-2 form-control-label' for='"+slugify(attrName)+"'>"+attrName+"</label>" +
                "<div class='col-md-7'>" +
                    "<input class='form-control' type='text' name='"+slugify(attrName)+"[value]' id='"+slugify(attrName)+"[value]' placeholder='' required=''>" +
                    "<input type='hidden' name='"+slugify(attrName)+"[type]' id='"+slugify(attrName)+"[type]' placeholder='' required='' value='text'>" +
                "</div>" +
            "</div>";
    return html;
}

function render_number_AttrHtml(attrName)
{
    return "<div class='form-group row'>" +
        "<label class='col-md-2 form-control-label' for='"+slugify(attrName)+"'>"+attrName+"</label>" +
        "<div class='col-md-7'>" +
        "<input class='form-control' type='number' name='"+slugify(attrName)+"[value]' id='"+slugify(attrName)+"[value]' placeholder='' required=''>" +
        "<input type='hidden' name='"+slugify(attrName)+"[type]' id='"+slugify(attrName)+"[type]' placeholder='' required='' value='number'>" +
        "</div>" +
        "</div>";
}

function render_rich_AttrHtml(attrName)
{
    return "<div class='form-group row'>" +
        "<label class='col-md-2 form-control-label' for='"+slugify(attrName)+"'>"+attrName+"</label>" +
        "<div class='col-md-7'>" +
        "<textarea class='form-control' type='number' name='rich_"+slugify(attrName)+"' id='rich_"+slugify(attrName)+"' placeholder='' required=''>" +
        "<input type='hidden' name='"+slugify(attrName)+"[type]' id='"+slugify(attrName)+"[type]' placeholder='' required='' value='rich'>" +
        "</div>" +
        "</div>";
}

