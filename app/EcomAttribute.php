<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcomAttribute extends Model
{
	protected $table = 'ecom_attribute';
	public function insertAttr(array $attrs)
	{
	    $results = [];
		foreach($attrs as $key=>$value)
		{
			$checkExist = $this->where('name',trim($key))->first();
			if($checkExist==null){
				$newEcomProductAtt  = new $this;
				$newEcomProductAtt->name = trim($key);
				$newEcomProductAtt->type = trim($value['type']);
				$newEcomProductAtt->save();
                $results[] = $newEcomProductAtt->id;
			}else{
                $results[] = $checkExist->id;
            }

		}
		return $results;
	}
}
