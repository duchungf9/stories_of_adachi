<?php
namespace App\Http\Controllers\Backend\Ecom;
use App\EcomCategory;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
class EcomCategoryController  extends Controller{
	protected $ecomCat;
	function __construct(){
		if(!$this->ecomCat instanceof EcomCategory){
			$this->ecomCat = new EcomCategory();
		}
	}
	
	/**
	 * @param array $request
	 * @param array $rules
	 * @param array $messages
	 *
	 * @return mixed
	 */
	private function validator(array $request,array $rules, array $messages = []){
		return Validator::make($request,$rules,$messages);
	}
	
	/**
	 * @return mixed
	 */
	private function validateRequest($id=null){
		$rules = $this->ecomCat->getRules(request()->method(),$id);
		$validator = $this->validator(request()->all(),$rules);
		if($validator->fails() == true){
			return $validator->messages()->messages();
		}
		return $validator->passes();
	}
	
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index()
	{
		$params['categories'] = EcomCategory::getListCategories(['is_active'=>1],10);
		return view('backend.ecom.category.index',compact('params'));
	}
	
	public function create()
	{
		$params = [];
		$params['categories'] = EcomCategory::getListCategories(['is_active'=>1,'parent_id'=>NULL]);
		return view('backend.ecom.category.store',compact('params'));
	}
	
	public function store(){
		$validateResults = $this->validateRequest();
		if(is_array($validateResults)){
			return redirect()->route('admin.ecom_category.create')->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		if($validateResults===true){
			$insert = ($this->ecomCat->insertEcomCat(request()->except("_token")));
			if($insert){
				return redirect()->route('admin.ecom_category.index');
			}
		}
	}
	
	public function show(int $id)
	{
		$params = [];
		$params['categories'] = EcomCategory::getListCategories(['is_active'=>1,'parent_id'=>null]);
		$params['category'] = $this->ecomCat->where('id',$id)->first();
		return view('backend.ecom.category.show',compact('params'));
	}
	
	public function update(int $id){
		if(!request()->has('is_active')){
			request()->merge(['is_active'=>0]);
		}
		$eComCat = $this->ecomCat->where('id',$id)->first();
		if($eComCat==null){
			return redirect()->route('admin.ecom_category.index');
		}
		$validateResults = $this->validateRequest($eComCat->id);
		if(is_array($validateResults)){
			return redirect()->route('admin.ecom_category.show',['id'=>$eComCat->id])->withErrors($validateResults)->withInput(request()->except("_token"));
		}
		$update = $this->ecomCat->updateEcomCat(request()->except(["_token","_method"]),$eComCat);
		if($update)
		{
			return redirect()->route('admin.ecom_category.show',['id'=>$eComCat->id]);
		}
		
	}
	
	public function destroy(int $id)
	{
		$delete = $this->ecomCat->where('id',$id)->delete();
		if($delete){
			return redirect()->route('admin.ecom_category.index');
			
		}
	}
	
	
	
	
	
}