@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
    @php
        extract($params);
    @endphp
    {{ html()->form('POST', route('admin.category.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Category Management
                        <small class="text-muted">Create Category</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            
            <hr />
            
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label('Title')->class('col-md-2 form-control-label')->for('title') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->placeholder('title of category')
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
    
                    <div class="form-group row">
                        {{ html()->label('Slug')->class('col-md-2 form-control-label')->for('slug') }}
                        <div class="col-md-10">
                            {{ html()->text('slug')
                                ->class('form-control')
                                ->placeholder('slug of category')
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                    
                    <div class="form-group row">
                        {{ html()->label("Description")->class('col-md-2 form-control-label')->for('description') }}
                        
                        <div class="col-md-10">
                            {{ html()->text('description')
                                ->class('form-control')
                                ->placeholder('description')
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.auth.user.index'), __('buttons.general.cancel')) }}
                </div><!--col-->
                
                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.create')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
    <script src="/js/backend/ecom/category.js"></script>
@endpush
