<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcomAttributeValue extends Model
{
	protected $table = 'ecom_attribute_value';
	
	public function insertAttrValue(array $attrs, array $attrIds)
	{
	    $ids = [];
		foreach($attrs as $key=>$value)
		{
			$attr_id = EcomAttribute::where('name',$key)->first();
			$exsist  = $this->where('ecom_attribute_id',$attr_id->id)->whereIn('id',$attrIds)->first();
			if($exsist==null){
				$newAttrValue = new $this;
			}else{
				$newAttrValue = $exsist;
			}
			$newAttrValue->ecom_attribute_id = $attr_id->id;
			$newAttrValue->value = $value['value'];
			$newAttrValue->save();
            $ids[] = $newAttrValue->id;
		}
		return $ids;
	}

	public function EcomAttr()
    {
        return $this->belongsTo(EcomAttribute::class,'ecom_attribute_id','id');
    }
	
}
