@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
	<?php
	extract($params);
	?>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Article Management
                        <small class="text-muted">Actived Article</small>
                    </h4>
                </div><!--col-->
                
                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
                        <a href="{{ route('admin.article.create') }}" class="btn btn-success ml-1"
                           data-toggle="tooltip" title="Create New"><i class="fa fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->
            
            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Slug</th>
                                <th>description</th>
                                <th>created at</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($articles as $article)
                                <tr>
                                    <td>{{ $article->title }}</td>
                                    <td>{{ $article->slug }}</td>
                                    <td>{{ $article->description }}</td>
                                    <td>{{ $article->created_at }}</td>
                                    <td>
                                        <div class="btn-group btn-group-sm" role="group" aria-label="User Actions">
                                            <a href="{!! route('admin.article.edit',['id'=>$article->id]) !!}" class="btn btn-info"><i
                                                        class="fa fa-search" data-toggle="tooltip" data-placement="top"
                                                        title="" data-original-title="View"></i></a>
                                            <a href="{!! route('admin.article.edit',['id'=>$article->id]) !!}"
                                               class="btn btn-primary"><i class="fa fa-pencil" data-toggle="tooltip"
                                                                          data-placement="top" title=""
                                                                          data-original-title="Edit"></i></a>
                                            <div class="btn-group" role="group">
                                                <button id="userActions" type="button"
                                                        class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                                                        aria-haspopup="true" aria-expanded="false">
                                                    More
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="userActions">
                                                    {{ html()->form('POST', route('admin.article.destroy',['id'=>$article->id]))->class('form-horizontal')->open() }}
                                                    {{ method_field('DELETE') }}
                                                    {{ form_submit(__('buttons.general.crud.delete')) }}
                                                    {{ html()->form()->close() }}
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                    
                    </div>
                </div><!--col-->
                
                <div class="col-5">
                    <div class="float-right">
                        {!! $articles->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->

@endsection
