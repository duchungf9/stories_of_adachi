<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEcomCategorySpecAttr extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('ecom_category_spec_attr', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('value')->nullable();
			$table->string('type')->nullable();
			$table->integer('ecom_cat_id')->nullable();
			$table->timestamps();
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
