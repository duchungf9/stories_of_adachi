@extends ('backend.layouts.app')

@section ('title', 'Caterory - LIST')

@section('breadcrumb-links')

@endsection

@section('content')
    @php
        extract($params);
    @endphp
    {{ html()->form('POST', route('admin.article.update',['id'=>$article->id]))->class('form-horizontal')->open() }}
    {{ method_field('PATCH') }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Article Management
                        <small class="text-muted">Article Edit</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->
            
            <hr />
            
            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label('Parent Category')->class('col-md-2 form-control-label')->for('category_id') }}
                        <div class="col-md-10">
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="">--</option>
                            @foreach ($categories as $categoryChild)
                                    <option value="{{ $categoryChild->id }}"
                                    @if($categoryChild->id == $article->category->id)
                                        selected
                                    @endif
                                    >{{ $categoryChild->name }}</option>
                                    @if(count($categoryChild->child_ecom_category)>0)
                                        @foreach($categoryChild->child_ecom_category as $child)
                                            <option value="{!! $child->id !!}"
                                                    @if($child->id == $article->category->id)
                                                        selected
                                                    @endif
                                            >-{!! $child->name !!}</option>
            
                                        @endforeach
        
                                    @endif
                            @endforeach
                            </select>
                        </div><!--col-->
                    </div><!--form-group-->
    
                    <div class="form-group row">
                        {{ html()->label('Title')->class('col-md-2 form-control-label')->for('title') }}
        
                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->value($article->title)
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Slug')->class('col-md-2 form-control-label')->for('slug') }}
        
                        <div class="col-md-10">
                            {{ html()->text('slug')
                                ->class('form-control')
                                ->value($article->slug)
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('thumb')->class('col-md-2 form-control-label')->for('thumb') }}
                        <div class="col-md-8">
                            {{ html()->text('thumb')
                                ->class('form-control')
                                ->value($article->thumb)
                                ->attribute('maxlength', 191)
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->
                    <div class="form-group row">
                        {{ html()->label('Content')->class('col-md-2 form-control-label')->for('content') }}
                        <div class="col-md-10">
                            {{ html()->textarea('content')
                                ->class('form-control')
                                ->value($article->content)
                                ->attribute('maxlength', 191)
                                ->required()
                             }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
        
        <div class="card-footer clearfix">
            <div class="row">
                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection
@push('after-scripts')
<script src="/js/backend/ecom/Article.js"></script>
<script>
    CKEDITOR.replace('content');
</script>
@endpush
