<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EcomCategory extends Model
{
	protected $table = 'ecom_category';
	
	/**
	 * @param string $method
	 * @param null   $id
	 *
	 * @return array
	 */
	public function getRules(string $method,$id=null){
		switch($method)
		{
			case 'GET':
			case 'DELETE':
			{
				return [];
			}
			case 'POST':
			{
				return [
					'name' => 'required',
					'alias'  => 'required|unique:ecom_category'
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				return [
					'name' => 'required',
					'alias'  => 'required|unique:ecom_category,alias,'.$id,
				];
			}
			default:break;
		}
	}
	
	/**
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function attributes(){
		return $this->hasMany(EcomCategorySpecAttr::class,'ecom_cat_id','id');
	}
	
	
	/**
	 * @return mixed
	 */
	public function getChildEComCategoryAttribute(){
		$parentId = $this->id;
		$search = EcomCategory::where('parent_id',$parentId)->get();
		return $search;
	}
	
	/**
	 * @param array $filters
	 * @param bool  $paginate
	 *
	 * @return mixed
	 */
	public static function getListCategories(array $filters = [],$paginate = false)
	{
		$query =  new self();
		
		if(!empty($filters)){
			$filter = [];
			foreach($filters as $key=>$value)
			{
				$filter[] = [$key, '=',$value];
			}
			$query =  $query->where($filter);
		}
		if($paginate!=false){
			return $query->paginate($paginate);
		}
		return $query->get();
	}
	
	/**
	 * @param array $params
	 *
	 * @return bool
	 */
	public function insertEcomCat(array $params)
	{
		$newCat = new $this;
		foreach($params as $field=>$value){
			$this->$field = $value;
		}
		return $this->save();
	}
	
	/**
	 * @param array             $params
	 * @param \App\EcomCategory $ecomCategory
	 *
	 * @return bool
	 */
	public function updateEcomCat(array $params,EcomCategory $ecomCategory)
	{
		foreach($params as $field=>$value){
			$ecomCategory->$field = $value;
		}
		return $ecomCategory->save();
	}
	
	
}
